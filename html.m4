divert(-1)
define(JS, `<script src="$1"></script>')
define(CSS, `<link href="$1" rel="stylesheet">')
define(TITLE_TEMPLATE, `<title id="title"></title>')
divert(0)dnl
